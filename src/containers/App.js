import React, { Component } from 'react';

// Import Components
import List from "../components/List/index";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    componentDidMount() {
        // Load json file and set it to the state
        fetch('./../../data.json')
            .then((res) => res.json())
            .then((data) => {
                this.setState({data: data});
            });
    }


    render() {
        return (
           <main className="app">
                <List data={this.state.data}/>
           </main>
        )
    }
}
export default App