import React, { Component } from 'react';
import Moment from 'moment';

class ListItems extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null,
        }
    }

    componentWillMount() {
        // Set the initial state for the data
        this.sortData();
    }

    /**
     * sortData
     * filter data if you have applied a filter in the dropdown menu
     *
     * @param filter
     * filter will ether be left undefined or be one of the following complete, incomplete, cancelled
     */

    sortData(filter) {
        const { data } = this.props;

        switch (filter) {
            case "complete":
                this.setState({
                    data: data.filter(items => items.status === "complete")
                });
                break;
            case "incomplete":
                this.setState({
                    data: data.filter(items => items.status === "incomplete")
                });
                break;
            case "cancelled":
                this.setState({
                    data: data.filter(items => items.status === "cancelled")
                });
                break;
            default:
                this.setState({data});
        }

    }

    handleFilter(e) {
        this.sortData(e.target.value);
    }

    renderItems() {
        Moment.locale('en');

        return this.state.data.map((item, key) => {
           return (
               <div className="list-container__item flex-container" key={key}>
                   <div className="list-container__details column">
                       <div className="list-container__title">{item.location}</div>
                       <div className="list-container__date">{Moment(item.startDate).format('LLL')}</div>
                   </div>
                   <div className="column column--valign">
                       <div className="list-container__status">{item.status}</div>
                   </div>
               </div>
           )
        });
    }

    render() {
        return (
            <div className="list-container">
                <h1>Lessons list</h1>
                <div className="list-container__filters">
                    <span>Filter by status</span>
                    <select onChange={this.handleFilter.bind(this)}>
                        <option value="--">--</option>
                        <option value="complete">Complete</option>
                        <option value="incomplete">Incomplete</option>
                        <option value="cancelled">Cancelled</option>
                    </select>
                </div>
                {this.renderItems()}
            </div>
        )
    }
}
export default ListItems;