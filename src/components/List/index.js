import React, { Component } from 'react';

import ListItems from './Item';

class List extends Component {

    constructor(props) {
        super(props);
        this.state = {
            lessons: null
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data !== this.state.lessons) {
            this.setState({ lessons: nextProps.data });
        }
    }

    render() {
        if (this.state.lessons) {
            return (
                <div className="list">
                    <ListItems data={this.state.lessons}/>
                </div>
            )
        }else{
            return <div className="list">Waiting for data</div>
        }
    }
}
export default List;